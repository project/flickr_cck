
/**********/
 Flickr CCK
/**********/

Authors: Aaron Winborn & Sam Tresler
Original Development at DrupalCampNYC2
01/21/07

Requires: Drupal 4.7+, cck & flickr modules
Optional: Views

This module will install CCK fields for Flickr Flash Slideshows and
Flickr Flash Badges.

If the Slideshow Creator module is installed, a third format for
slideshows through its api (called 'API Slideshow' in the flickr_cck
widgets) will be made available, and is easier to customize, since
it's a javascript/html slideshow.

Questions can be directed to winborn at advomatic dot com
